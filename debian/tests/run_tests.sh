set -e
cp -a tests $AUTOPKGTEST_TMP
for py in $(py3versions -r 2>/dev/null); do 
  cd "$AUTOPKGTEST_TMP"
  echo "Testing with $py:"
  $py -m unittest discover -v -s tests/ -p '*_test.py'
done
